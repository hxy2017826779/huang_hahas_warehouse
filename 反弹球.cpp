#include <conio.h>
#include <graphics.h>
#include<time.h>
#include<stdio.h>

#define High 600  //游戏画面尺寸
#define Width 740
#define Brick_num 80 // 砖块个数
#define moneynum 10 //金币个数

// 全局变量
int ball_x, ball_y; // 小球的坐标
int ball_vx, ball_vy; // 小球的速度
int radius;  // 小球的半径
int bar_x, bar_y; // 挡板中心坐标
int bar_high, bar_width;  // 挡板的高度和宽度
int bar_left, bar_right, bar_top, bar_bottom; // 挡板的上下左右位置坐标
static int money_x[moneynum], money_y[moneynum]; //金币的中心位置
static int left[moneynum], right[moneynum], top[moneynum], bottom[moneynum]; //金币的上下左右位置坐标
int score; //得分

int isBrickExisted[Brick_num]; // 每个砖块是否存在，1为存在，0为没有了
int brick_high, brick_width; // 每个砖块的高度和宽度

void startup()  // 数据初始化
{
	srand((unsigned)(time(NULL)));
	ball_x = Width / 2;
	ball_y = High / 2;
	ball_vx = 1;
	ball_vy = 1;
	radius = 10;
	score = 0;

	bar_high = High / 30;
	bar_width = Width / 2;
	bar_x = Width / 2;
	bar_y = High - bar_high / 2;
	bar_left = bar_x - bar_width / 2;
	bar_right = bar_x + bar_width / 2;
	bar_top = bar_y - bar_high / 2;
	bar_bottom = bar_y + bar_high / 2;

	brick_width = Width / 20 ;
	brick_high = High / 20;

	int i;
	for (i = 0; i < Brick_num; i++)
		isBrickExisted[i] = rand()%2+1; //随机生成藏有金币的砖块，1为普通砖块，2为藏有金币的砖块

	initgraph(Width, High);
	BeginBatchDraw();
}

void clean()  // 消除画面
{
	setcolor(BLACK);
	setfillcolor(BLACK);
	fillcircle(ball_x, ball_y, radius); 	// 绘制黑线、黑色填充的圆
	bar(bar_left, bar_top, bar_right, bar_bottom);	// 绘制黑线、黑色填充的挡板

	int i, brick_left, brick_right, brick_top, brick_bottom,item_1,item_2;
	for (i = 0; i < Brick_num; i++)
	{;
		if (i < 20)
		{
			item_1 = i;
			item_2 = 1;
		}
		else if (i >= 20 && i < 40)
		{
			item_1 = i - 20;
			item_2 = 2;
		}
		else if (i >= 40 && i < 60)
		{
			item_1 = i - 40;
			item_2 = 3;
		}
		else
		{
			item_1 = i - 60;
			item_2 = 4;
		}
		brick_left = item_1 * brick_width;
		brick_right = brick_left + brick_width;
		brick_top = (i/20)*brick_high;
		brick_bottom = item_2 * brick_high;

		if (!isBrickExisted[i])	 // 砖块没有了，绘制黑色
			fillrectangle(brick_left, brick_top, brick_right, brick_bottom);
	}

	//清除上一秒金币
	for (i = 0; i < moneynum; i++)
	{
		if (money_x[i] > 0)
			fillellipse(left[i], top[i], right[i], bottom[i]);
	}
}

void show()  // 显示画面
{
	setcolor(YELLOW);
	setfillcolor(GREEN);
	fillcircle(ball_x, ball_y, radius);	// 绘制黄线、绿色填充的圆
	bar(bar_left, bar_top, bar_right, bar_bottom);	// 绘制黄线、绿色填充的挡板

	int i, brick_left, brick_right, brick_top, brick_bottom,item_1,item_2;

	for (i = 0; i < Brick_num; i++)
	{
		if (i < 20)
		{
			item_1 = i;
			item_2 = 1;
		}
		else if (i >= 20 && i < 40)
		{
			item_1 = i - 20;
			item_2 = 2;
		}
		else if (i >= 40 && i < 60)
		{
			item_1 = i - 40;
			item_2 = 3;
		}
		else
		{
			item_1 = i - 60;
			item_2 = 4;
		}
		brick_left = item_1 * brick_width;
		brick_right = brick_left + brick_width;
		brick_top = (i/20)*brick_high;
		brick_bottom = item_2 * brick_high;

		if (isBrickExisted[i])	 // 砖块存在，绘制砖块
		{
			setcolor(WHITE);
			setfillcolor(RED);
			fillrectangle(brick_left, brick_top, brick_right, brick_bottom);	// 绘制砖块
		}
	}

	setcolor(GREEN);
	setfillcolor(YELLOW); //准备绘制金币
	for (i = 0; i < moneynum; i++)
	{
		if(money_x[i]>0)
			fillellipse(left[i], top[i], right[i], bottom[i]);  //绘制金币
	}

	FlushBatchDraw();
	// 延时
	Sleep(5);
}

void updateWithoutInput()  // 与用户输入无关的更新
{
	// 挡板和小圆碰撞，小圆反弹
	if (ball_x + radius >= bar_left && ball_x - radius <= bar_right 
		&& ball_y + radius >= bar_top && ball_y - radius <= bar_bottom)
			ball_vy = -ball_vy;

	// 更新小圆坐标
	ball_x = ball_x + ball_vx;
	ball_y = ball_y + ball_vy;

	//更新金币坐标
	for (int j = 0; j < moneynum; j++)
	{
		if (money_x[j] > 0)
		{
			top[j] = money_y[j] - 10;
			bottom[j] = money_y[j] + 10;
			money_y[j] = money_y[j] + 1;
		}
	}

	// 小圆和边界碰撞
	if ((ball_x == radius) || (ball_x == Width - radius))
		ball_vx = -ball_vx;
	if ((ball_y == radius) || (ball_y == High - radius))
		ball_vy = -ball_vy;

	// 判断小圆是否和某个砖块碰撞
	int i, brick_left, brick_right, brick_bottom,flag=0;
	for (i = 0; i < Brick_num; i++)
	{
		if (isBrickExisted[i]&&i<20)	 // 砖块存在，才判断
		{
			brick_left = i * brick_width;
			brick_right = brick_left + brick_width;
			brick_bottom = brick_high;
			flag = 1;
		}
		else if (isBrickExisted[i]&&i>=20&&i<40)
		{
			brick_left = (i-20) * brick_width;
			brick_right = brick_left + brick_width;
			brick_bottom = 2*brick_high;
			flag = 1;
		}
		else if (isBrickExisted[i] && i >= 40&&i<60)
		{
			brick_left = (i - 40) * brick_width;
			brick_right = brick_left + brick_width;
			brick_bottom = 3 * brick_high;
			flag = 1;
		}
		else if (isBrickExisted[i] && i >= 60)
		{
			brick_left = (i - 60) * brick_width;
			brick_right = brick_left + brick_width;
			brick_bottom = 4 * brick_high;	
			flag = 1;
		}

		if (flag == 1)  //砖块存在，才判断
		{
			if ((ball_y == brick_bottom + radius) && (ball_x >= brick_left) && (ball_x <= brick_right)
				|| (ball_y <= brick_bottom + radius) && (ball_x == brick_left - radius)
				|| (ball_y <= brick_bottom + radius) && (ball_x == brick_right + radius))
			{
				if (isBrickExisted[i] == 2) //如果是藏有金币的砖块则记录下金币的位置
				{
					for (int j = 0; j < moneynum; j++)
						if (money_x[j] == 0)
						{
							money_x[j] = brick_right - brick_width / 2;
							money_y[j] = brick_bottom - brick_high / 2;
							left[j] = money_x[j] - 5;
							top[j] = money_y[j] - 10;
							right[j] = money_x[j] + 5;
							bottom[j] = money_y[j] + 10;
							break;
						}
				}
				isBrickExisted[i] = 0;
				ball_vy = -ball_vy;
				flag = 0;
			}
		}
	}

	
	for (int j = 0; j < moneynum; j++)
	{
		if (money_x[j] > 0 &&bottom[j]==bar_top&&left[j]>=bar_left&&right[j]<=bar_right) //金币被接住
		{
			money_x[j]=0;
			score++;
		}
		if (money_x[j] > 0 && bottom[j] == High) //金币掉落消失
		{
			money_x[j] = 0;
		}
	}

}

void updateWithInput()  // 与用户输入有关的更新
{
	char input;
	if (_kbhit())  // 判断是否有输入
	{
		input = _getch();  // 根据用户的不同输入来移动，不必输入回车
		if (input == 'a' && bar_left > 0)
		{
			bar_x = bar_x - 15;  // 位置左移
			bar_left = bar_x - bar_width / 2;
			bar_right = bar_x + bar_width / 2;
		}
		if (input == 'd' && bar_right < Width)
		{
			bar_x = bar_x + 15;  // 位置右移
			bar_left = bar_x - bar_width / 2;
			bar_right = bar_x + bar_width / 2;
		}
		if (input == 'w' && bar_top > 0)
		{
			bar_y = bar_y - 15;  // 位置左移
			bar_top = bar_y - bar_high / 2;
			bar_bottom = bar_y + bar_high / 2;
		}
		if (input == 's' && bar_bottom < High)
		{
			bar_y = bar_y + 15;  // 位置右移
			bar_top = bar_y - bar_high / 2;
			bar_bottom = bar_y + bar_high / 2;
		}
		if (input == ' ')
		{
			system("pause");
		}
	}
}

void gameover()
{
	EndBatchDraw();
	closegraph();
}

int main()
{
	startup();  // 数据初始化	
	while (1)  //  游戏循环执行
	{
		clean();  // 把之前绘制的内容取消
		updateWithoutInput();  // 与用户输入无关的更新
		updateWithInput();     // 与用户输入有关的更新
		show();  // 显示新画面
	}
	gameover();     // 游戏结束、后续处理
	return 0;
}

